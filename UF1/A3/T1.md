Crear i validar documents XHTML
===============================

MP4UF1A3T1

Introducció al vocabulari HTML amb sintaxi XML

Els vocabularis HTML (HyperText Markup Language)
------------------------------------------------

L’HTML és el llenguatge de publicació de la Web.

-   Al llarg de la història s’han succeït diferents versions de l’HTML.
    La més antiga i important de les encara vigents és l’HTML4.
-   L’HTML4 és una aplicació del
    [SGML](http://en.wikipedia.org/wiki/Standard_Generalized_Markup_Language),
    conforme a l’estàndard internacional ISO 8879 — Standard Generalized
    Markup Language.
-   L’HTML4 en concreta en tres diferents DTDs: *Frameset*,
    *Transitional*, *Strict*.
-   El DTD preferent és el de tipus estricte, el de menor quantitat
    d’elements i atributs, on la presentació queda en mans dels fulls
    d’estil CSS.
-   L’XHTML 1.0 és com HTML4 però amb sintaxi XML, més restringida que
    la del SGML.
-   El vocabulari HTML5, encara en fase de desenvolupament, es fonamenta
    en l’XHTML 1.0 estricte.
-   El format EPUB (acrònim en anglès de *electronic publication*
    –publicació electrònica–) és un estàndard obert de llibre electrònic
    fonamentat en XHTML.

XHTML: HTML amb sintaxi XML
---------------------------

L’XHTML és una reformulació de l’HTML amb sintaxi XML, més estricte i
simple que la del SGML.

-   Per compatibilitat amb els navegadors ometrem la declaració XML.
-   Declaració DOCTYPE de l’XHTML estricte:

        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

-   Disposem d’una còpia local dels DTDs per XHTML proporcionada pel
    paquet `xhtml1-dtds` de Fedora.

Estructura dels documents HTML {#TEMPLATE}
------------------------------

-   Exemple de document XHTML bàsic (elements i atributs sempre en
    minúscules):

        <!DOCTYPE html 
            PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
          <head>
            <title>Virtual Library</title>
          </head>
          <body>
            <p>Moved to <a href="http://example.org/">example.org</a>.</p>
          </body>
        </html>

Codificació de caràcters dels documents HTML
--------------------------------------------

-   La codificació per defecte és ISO-8859-1 (latin1).
-   Per establir la codificació UTF-8 cal afegir aquest element dins
    l’element `head`:

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

-   L’HTML4 defineix moltes entitats generals per caràcters no sempre
    presents en el teclat (per exemple): `&hellip`; (…) `&raquo`; (»)
    `&laquo`; («) `&rsquo`; (’) `&lsquo`; (‘) `&rdquo`; (”)
    `&ldquo`; (“) `&mdash`; (—) `&ndash`; (–) `&nbsp;` ( ).
-   Utilitzant entitats de caràcters numèriques podem introduir en els
    documents XML qualsevol caràcter Unicode.

**Nota**: en el DTD de l’XHTML la definició d’entitats generals per a
caràcters es fa en fitxers auxiliars amb l’extensió `.ent`.

Diferències del XHTML 1.0 estricte en relació l’HTML5
-----------------------------------------------------

Les diferències entre l’XHTML 1.0 estricte l’HTML5 són petites, i en la
pràctica podem considerar aquest últim com una ampliació directa del
primer. Per tant tot el que puguem dir de l’XHMTL segueix sent
aplicable, fora de petits novetats com ara:

-   La declaració `DOCTYPE` de l’HTML5 queda substituïda per una
    d’aquestes dues opcions:
    -   `<!DOCTYPE html>`
    -   `<!DOCTYPE html SYSTEM "about:legacy-compat">`
-   No hi ha per tant DTD i no és possible validar els documents
    (excepte amb [The Nu Html
    Checker](https://github.com/validator/validator/releases)).
-   La sintaxi de l’HTML5 es presenta en dues variants: una similar a la
    de l’HTML tradicional i la del XHTML5, que és XML ben format.
-   La codificació de caràcters es defineix amb la construcció
    `<meta charset="UTF-8">`.
-   Desapareixen els elements `acronym` i `tt`.
-   Apareixen nous elements, com ara `section`, `article`, `audio` i
    `video`.
-   Reapareix l’element `iframe`.
-   Es poden inserir directament els vocabularis SVG i MathML.
-   Hi ha moltes novetats pel que fa al JavaScript, que combinen
    visualment amb les novetats del CSS3.

Si us voleu penedir per haver preguntat podeu llegir tots els detalls en
el document [HTML5 Differences from
HTML4](http://www.w3.org/TR/html5-diff/).

Com podem convertir un document XHTML estricte a HTML5? Tot el que cal
fer és:

1.  Canviar la declaració `DOCTYPE`.
2.  Canviar la declaració de codificació de caràcters.
3.  Canviar l'element `acronym` per `abbr`.
4.  Canviar l'element `tt` per `span` més CSS.

Això és tot! Naturalment, a continuació es pot afegir al document
convertit qualsevol de les novetats, usar `iframe`, etc.

Enllaços recomanats
-------------------

-   [Tutorial d’XHTML](http://manual-xhtml.blogspot.com.es/)
-   [Wikipedia: XHTML](http://es.wikipedia.org/wiki/XHTML)
-   [Wikipedia: HTML 4.0](http://es.wikipedia.org/wiki/HTML_4.0)
-   [Common HTML entities used for
    typography](http://www.w3.org/wiki/Common_HTML_entities_used_for_typography)
-   [Benefits of polyglot
    XHTML5](http://www.xmlplease.com/xhtml/xhtml5polyglot/)

Pràctiques
----------

-   Practica els tutorials citats en els enllaços recomanats.
-   Per fer servir al llarg del curs, prepara una “plantilla” de
    document XHTML prenent com a model el document bàsic presentat més
    amunt, amb codificació UTF-8 i ben comentat.
-   Per tenirles a ma, afegeix al document les entitats generals més
    importants (mira els fitxers amb extensió `.ent` del DTD).
-   Explora les prestacions de l’accessori del Gnome Character Map per
    facilitar el treball amb caràcters no presents al teclat.
-   Si no ho has fet abans, instal·la el paquet `xhtml1-dtds`.
-   Cerca dins del DTD de l’XHTML estricte les declaracions dels
    principals elements HTML.

